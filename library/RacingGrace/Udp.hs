module RacingGrace.Udp where

import Control.Concurrent.BoundedChan
import Control.Exception (bracket)
import Control.Monad (unless, void)
import qualified Data.ByteString.Char8 as BS
import Data.List (intersperse)
import Data.Time.Clock (UTCTime, getCurrentTime)
import qualified Network.Socket as S
import qualified Network.Socket.ByteString as S

data Packet = Packet {
  payload :: BS.ByteString,
  origin :: String,
  received :: UTCTime
}

runConsumer :: BoundedChan Packet -> Int -> IO ()
runConsumer q port = bracket createSocket S.close acceptConnections
  where
    createSocket = do
      serverAddr <- fmap head $ S.getAddrInfo
        (Just (S.defaultHints {S.addrFlags = [S.AI_PASSIVE]}))
        Nothing
        (Just $ show port)
      socket <- S.socket (S.addrFamily serverAddr) S.Datagram S.defaultProtocol
      S.bind socket $ S.addrAddress serverAddr
      pure socket

    acceptConnections socket = do
      (payload, remote) <- S.recvFrom socket port
      let origin = case remote of
            S.SockAddrInet _ a -> showHostAddress a
            S.SockAddrInet6 _ _ a _ -> show a
            S.SockAddrUnix _ -> "127.0.0.1"

          -- would have been nice to use showHostAddress{6} from network, but it's hidden
          showHostAddress :: S.HostAddress -> String
          showHostAddress ip =
            let (u3, u2, u1, u0) = S.hostAddressToTuple ip in
            concat . intersperse "." $ map show [u3, u2, u1, u0]

      time <- getCurrentTime

      unless (BS.null payload) $
        -- we intentionally drop messages if the queue is full. If we would
        -- rather backpressure on the network stack, change to writeChan
        void $ tryWriteChan q (Packet payload origin time)

      acceptConnections socket
