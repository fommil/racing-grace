{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- generic data format
module RacingGrace.Telemetry where

import Data.Functor.Contravariant (contramap, (>$<))
import Data.Int (Int64)
import Data.Text (Text)
import Data.Time (UTCTime, utc, utcToLocalTime)
import Data.UUID (UUID)
import qualified Hasql.Encoders as SQL

newtype Session = Session UUID deriving newtype (Eq, Ord, Show)

sqlSession :: SQL.Value Session
sqlSession = contramap (\(Session uuid) -> uuid) SQL.uuid

newtype Frame = Frame Int64 deriving newtype (Eq, Ord, Show)

sqlFrame :: SQL.Value Frame
sqlFrame = contramap (\(Frame id) -> id) SQL.int8

-- DataKinds might be cleaner than this adhoc sum type
data Telemetry = TPosition Position
               | TVelocity Velocity
               | TTime Time
               | TUser User
               | TTrack Track
               | TInput Input
               | TDistance Distance
  deriving (Eq, Show)

-- ghost data doesn't typically come with additional telemetry, but is useful
-- for comparison (e.g. can provide the ideal racing line). It results in a lot
-- of duplicate data that can be cleaned up on a schedule by deduping based on
-- time within the lap (and across sessions on the same track, iff it is the
-- same ghost).
data Position = Position {
  received :: UTCTime,
  sess :: Session,
  frame :: Frame,
  ghost :: Maybe Text,
  x :: Float,
  y :: Float,
  z :: Float
} deriving (Eq, Ord, Show)

toSqlPosition :: SQL.Params Position
toSqlPosition =
  let p1 (Position p _ _ _ _ _ _) = p
      p2 (Position _ p _ _ _ _ _) = p
      p3 (Position _ _ p _ _ _ _) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable sqlFrame)) <>
      (ghost >$< SQL.param (SQL.nullable SQL.text)) <>
      (x >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (y >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (z >$< SQL.param (SQL.nonNullable SQL.float4))

-- Velocity can be estimated from Position (and if accuracy is good enough we may choose to ignore this data)
data Velocity = Velocity {
  received :: UTCTime,
  sess :: Session,
  frame :: Frame,
  vx :: Float,
  vy :: Float,
  vz :: Float
} deriving (Eq, Ord, Show)

toSqlVelocity :: SQL.Params Velocity
toSqlVelocity =
  let p1 (Velocity p _ _ _ _ _) = p
      p2 (Velocity _ p _ _ _ _) = p
      p3 (Velocity _ _ p _ _ _) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable sqlFrame)) <>
      (vx >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (vy >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (vz >$< SQL.param (SQL.nonNullable SQL.float4))

data Time = Time {
  received :: UTCTime,
  sess :: Session,
  frame :: Frame,
  lapnum :: Int,
  secs :: Float
} deriving (Eq, Ord, Show)

toSqlTime :: SQL.Params Time
toSqlTime =
  let p1 (Time p _ _ _ _) = p
      p2 (Time _ p _ _ _) = p
      p3 (Time _ _ p _ _) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable sqlFrame)) <>
      (fromIntegral . lapnum >$< SQL.param (SQL.nonNullable SQL.int8)) <>
      (secs >$< SQL.param (SQL.nonNullable SQL.float4))

data User = User {
  received :: UTCTime,
  sess :: Session,
  name :: Text
} deriving (Eq, Ord, Show)

toSqlUser :: SQL.Params User
toSqlUser =
  let p1 (User p _ _) = p
      p2 (User _ p _) = p
      p3 (User _ _ p) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable SQL.text))

data Track = Track {
  received :: UTCTime,
  sess :: Session,
  name :: Text
} deriving (Eq, Ord, Show)

toSqlTrack :: SQL.Params Track
toSqlTrack =
  let p1 (Track p _ _) = p
      p2 (Track _ p _) = p
      p3 (Track _ _ p) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable SQL.text))

data Input = Input {
  received :: UTCTime,
  sess :: Session,
  frame :: Frame,
  throttle :: Float,
  steer :: Float,
  brake :: Float,
  gear :: Int
} deriving (Eq, Ord, Show)

toSqlInput :: SQL.Params Input
toSqlInput =
  let p1 (Input p _ _ _ _ _ _) = p
      p2 (Input _ p _ _ _ _ _) = p
      p3 (Input _ _ p _ _ _ _) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable sqlFrame)) <>
      (throttle >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (steer >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (brake >$< SQL.param (SQL.nonNullable SQL.float4)) <>
      (fromIntegral . gear >$< SQL.param (SQL.nonNullable SQL.int8))

-- distance should be distance travelled along the track (relative to the center
-- of the track or the ideal racing line depending on the game's
-- implementation), not the physical distance. Can be estimated from Position if
-- we have a reference.
data Distance = Distance {
  received :: UTCTime,
  sess :: Session,
  frame :: Frame,
  distance :: Float
} deriving (Eq, Ord, Show)

toSqlDistance :: SQL.Params Distance
toSqlDistance =
  let p1 (Distance p _ _ _) = p
      p2 (Distance _ p _ _) = p
      p3 (Distance _ _ p _) = p
   in (utcToLocalTime utc . p1 >$< SQL.param (SQL.nonNullable SQL.timestamp)) <>
      (p2 >$< SQL.param (SQL.nonNullable sqlSession)) <>
      (p3 >$< SQL.param (SQL.nonNullable sqlFrame)) <>
      (distance >$< SQL.param (SQL.nonNullable SQL.float4))
