{-# LANGUAGE ViewPatterns #-}

module RacingGrace.Replayer where

import Control.Applicative ((<|>))
import Control.Concurrent.BoundedChan
import Control.Monad (void)
import qualified Data.ByteString as BS
import Data.List (sort)
import Data.List.Extra (breakOn, dropPrefix)
import Data.Time (UTCTime(..), defaultTimeLocale, parseTimeM)
import RacingGrace.Recorder (dateFormat)
import RacingGrace.Udp (Packet(..))
import System.Directory (doesDirectoryExist, listDirectory)
import System.FilePath (pathSeparator, (</>))

runReplayer :: BoundedChan Packet -> FilePath -> IO ()
runReplayer q base = runReplayer' base
  where
    parseTimestamp :: String -> Maybe UTCTime
    parseTimestamp str = parseTimeM False defaultTimeLocale dateFormat str
      <|> parseTimeM False defaultTimeLocale "/%Y/%m/%d/%M-%S-%q.dat" str
      -- "legacy" format for old data

    runReplayer' f = do
      isDir <- doesDirectoryExist f
      if isDir
      then do
        fs <- sort <$> listDirectory f
        let qfs = (f </>) <$> fs
        void $ mapM_ runReplayer' qfs
      else case breakOn [pathSeparator] $ dropPrefix base f of
        (origin, (parseTimestamp -> Just ts)) -> do
          payload <- BS.readFile f
          let packet = Packet payload origin ts
          writeChan q packet
        _ -> putStrLn $ "ignoring unexpected file: " <> f
