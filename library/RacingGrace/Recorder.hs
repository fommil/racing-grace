
module RacingGrace.Recorder (runRecorder, dateFormat) where

import Control.Concurrent.BoundedChan
import qualified Data.ByteString as BS
import Data.Time.Format (defaultTimeLocale, formatTime)
import RacingGrace.Udp (Packet(..))
import System.Directory (createDirectoryIfMissing)
import System.FilePath (takeDirectory)

dateFormat :: String
dateFormat = "/%Y/%m/%d/%M/%S/%q.dat"

runRecorder :: BoundedChan Packet -> FilePath -> IO ()
runRecorder q dir = do
  (Packet payload origin ts) <- readChan q
  let file = dir <> origin <> formatTime defaultTimeLocale dateFormat ts
      parent = takeDirectory file
  createDirectoryIfMissing True parent
  BS.writeFile file payload
  runRecorder q dir
