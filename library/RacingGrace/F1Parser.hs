{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}

-- Parser for the F1 series of games. Only F1:2020 is supported.
--
-- https://forums.codemasters.com/topic/50942-f1-2020-udp-specification/
module RacingGrace.F1Parser (runF1Parser) where

import Control.Concurrent.BoundedChan
import Control.Monad (replicateM)
import Control.Monad.Extra (whenJust)
import Data.Binary.Get
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Data.Containers.ListUtils (nubOrdOn)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Int (Int16, Int8)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (mapMaybe, maybeToList)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8')
import Data.Time (UTCTime(..))
import Data.UUID (fromWords64)
import Data.Word (Word16, Word32, Word64, Word8)
import RacingGrace.Telemetry (Telemetry)
import qualified RacingGrace.Telemetry as Telemetry
import RacingGrace.Udp (Packet(..))

runF1Parser :: BoundedChan Packet -> BoundedChan Telemetry -> IO ()
runF1Parser input output = do
  -- state should be size-limited to avoid memory leaks
  states <- newIORef mempty
  runF1Parser' input output states

runF1Parser' :: BoundedChan Packet -> BoundedChan Telemetry -> IORef (Map Word64 F1State) -> IO ()
runF1Parser' input output ref = do
  packet <- readChan input
  whenJust (parse packet) $ \d -> do
    states <- readIORef ref
    let sess = m_sessionUID . header $ d
        state = update d $ M.findWithDefault (F1State False mempty) sess states
    writeIORef ref $ M.insert sess state states
    -- putStrLn $ show d
    let telemetry = convert (received packet) state d
    putStrLn $ show telemetry
    mapM_ (writeChan output) telemetry
  runF1Parser' input output ref

data F1State = F1State {
  active :: Bool,
  -- ghosts only have (x,y,z) for the frame, everything else is junk
  ghosts :: Map Text Int
}

update :: F1 -> F1State -> F1State
update F1{header, body} st = case body of
  Packet2 (Lap (List22 laps))->
    let status = fmap m_driverStatus $ atMay (fromIntegral $ m_playerCarIndex header) laps
    in st { active = (status == Just 1 || status == Just 4) }
  Packet4 (Participants _ (List22 ps)) ->
    -- our goal here is to detect ghosts, which can exist in various different
    -- places in the game. If there is a human driver (m_driverId >= 100) that
    -- is m_aiControlled (= 1) and has a non-empty username, count them as a
    -- ghost. However, note that the name may be duplicated in which case we
    -- pick the first of each name. Note also that ghosts are not counted in
    -- m_numActiveCars.
    let getGhost (i, p) = if isGhost p then Just (getText48 $ m_name p, i) else Nothing
        isGhost CarParticipant{m_aiControlled, m_driverId, m_name} =
          m_aiControlled == 1 && m_driverId >= 100 && (not . T.null) (getText48 m_name)
     in st { ghosts = M.fromList . nubOrdOn fst . mapMaybe getGhost $ indexed ps }
  _ -> st

indexed :: [a] -> [(Int, a)]
indexed xs = go 0 xs
  where
    go i (a : as) = (i, a) : go (i + 1) as
    go _ _      = []

atMay :: Int -> [a] -> Maybe a
atMay o xs = if o < 0 then Nothing else f o xs
    where f 0 (x : _) = Just x
          f i (_ : xs) = f (i - 1) xs
          f _ [] = Nothing

parse :: Packet -> Maybe F1
parse (Packet (LBS.fromStrict -> payload) _ _) = case runGetOrFail getF1 payload of
  Left _ -> Nothing
  Right (_, _, f1) -> Just f1

convert :: UTCTime -> F1State -> F1 -> [Telemetry]
convert received F1State{active, ghosts} F1{header, body} =
  if not active then [] else
  let sess = Telemetry.Session . (\w64 -> fromWords64 w64 w64) $ m_sessionUID header
      frame = Telemetry.Frame . fromIntegral $ m_frameIdentifier header
  in case body of
  Packet0 Motion{m_carMotion} ->
    let cars = getList22 m_carMotion
        position ghost CarMotion{m_worldPositionX = x, m_worldPositionY = y, m_worldPositionZ = z} =
          Telemetry.TPosition $ Telemetry.Position received sess frame ghost x y z
        velocity CarMotion{m_worldVelocityX = vx, m_worldVelocityY = vy, m_worldVelocityZ = vz} =
          Telemetry.TVelocity $ Telemetry.Velocity received sess frame vx vy vz
        driver = concatMap (\car -> [position Nothing car, velocity car]) $ atMay (playerCarIndex header) cars
        ghosts' = mapMaybe (\(name, i) -> fmap (position (Just name)) $ atMay i cars) $ M.toList ghosts
     in driver ++ ghosts'
  Packet1 Session{m_trackId} -> maybeToList $ fmap (Telemetry.TTrack . Telemetry.Track received sess) (M.lookup (fromIntegral m_trackId) trackNames)
  Packet2 (Lap (List22 laps)) ->
    let mkTelemetry CarLap{m_currentLapTime, m_currentLapNum, m_lapDistance} =
          [Telemetry.TTime $ Telemetry.Time received sess frame (fromIntegral m_currentLapNum) m_currentLapTime,
           Telemetry.TDistance $ Telemetry.Distance received sess frame m_lapDistance]
     in concatMap mkTelemetry $ atMay (playerCarIndex header) laps
  Packet4 (Participants _ (List22 cars)) ->
    let mkTelemetry CarParticipant{m_name} = [Telemetry.TUser $ Telemetry.User received sess (getText48 m_name)]
     in concatMap mkTelemetry $ atMay (playerCarIndex header) cars
  Packet6 Input{m_carTelemetryData} ->
    let mkTelemetry CarInput{m_throttle, m_steer, m_brake, m_gear} =
          [Telemetry.TInput $ Telemetry.Input received sess frame m_throttle m_steer m_brake (fromIntegral m_gear)]
     in concatMap mkTelemetry $ atMay (playerCarIndex header) (getList22 m_carTelemetryData)

data F1 = F1 { header :: PacketHeader, body :: PacketBody }
  deriving (Eq, Ord, Show)

-- data is encoded using unpadded little endian
class GetF1 a where
  getF1 :: Get a

instance GetF1 Int8 where
  getF1 = getInt8

instance GetF1 Int16 where
  getF1 = getInt16le

instance GetF1 Word8 where
  getF1 = getWord8

instance GetF1 Word16 where
  getF1 = getWord16le

instance GetF1 Word32 where
  getF1 = getWord32le

instance GetF1 Word64 where
  getF1 = getWord64le

instance GetF1 Float where
  getF1 = getFloatle

-- could use Nat / KnownNat / natVal here but this is simple and Haskell98
newtype List4 a = List4 [a] deriving newtype (Eq, Ord, Show)
newtype List20 a = List20 [a] deriving newtype (Eq, Ord, Show)
newtype List21 a = List21 [a] deriving newtype (Eq, Ord, Show)
newtype List22 a = List22 { getList22 :: [a] } deriving newtype (Eq, Ord, Show)

instance GetF1 a => GetF1 (List4 a) where
  getF1 = List4 <$> replicateM 4 getF1

instance GetF1 a => GetF1 (List20 a) where
  getF1 = List20 <$> replicateM 20 getF1

instance GetF1 a => GetF1 (List21 a) where
  getF1 = List21 <$> replicateM 21 getF1

instance GetF1 a => GetF1 (List22 a) where
  getF1 = List22 <$> replicateM 22 getF1

-- utf-8, null terminated
newtype Text48 = Text48 { getText48 :: Text } deriving newtype (Eq, Ord, Show)

instance GetF1 Text48 where
  getF1 = do
    (bs, _) <- BS.break (0 ==) <$> getByteString 48
    case decodeUtf8' bs of
      Left err -> fail $ show err
      Right txt -> pure . Text48 $ txt

-- packetId
--
-- 0 = Motion
-- 1 = Session
-- 2 = Lap
-- 3 = Event
-- 4 = Participants
-- 5 = Car Setups
-- 6 = Car Telemetry
-- 7 = Car Status
-- 8 = Final Classification
-- 9 = Lobby Info
data PacketBody =
    Packet0 Motion
  | Packet1 Session
  | Packet2 Lap
  -- Packet3 intentionally skipped
  | Packet4 Participants
  -- Packet5 intentionally skipped
  -- "Telemetry" renamed to "Input"
  | Packet6 Input
  -- Packet{7,8,9} intentionally skipped
    deriving (Eq, Ord, Show)

instance GetF1 F1 where
  getF1 = do
    header <- getF1
    F1 header <$> case m_packetId header of
      0 -> Packet0 <$> getF1
      1 -> Packet1 <$> getF1
      2 -> Packet2 <$> getF1
      4 -> Packet4 <$> getF1
      6 -> Packet6 <$> getF1
      _ -> fail "unsupported packetId"

data PacketHeader = PacketHeader {
  m_packetFormat :: Word16,
  m_gameMajorVersion :: Word8,
  m_gameMinorVersion :: Word8,
  m_packetVersion :: Word8,
  m_packetId :: Word8,
  m_sessionUID :: Word64,
  m_sessionTime :: Float,
  m_frameIdentifier :: Word32,
  m_playerCarIndex :: Word8,
  m_secondaryPlayerCarIndex :: Word8
} deriving (Eq, Ord, Show)
{- BOILERPLATE PacketHeader GetF1 -}
{- BOILERPLATE START -}
instance GetF1 PacketHeader where
  getF1 = PacketHeader <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

playerCarIndex :: PacketHeader -> Int
playerCarIndex = fromIntegral . m_playerCarIndex

data CarMotion = CarMotion {
  m_worldPositionX :: Float,
  m_worldPositionY :: Float,
  m_worldPositionZ :: Float,
  m_worldVelocityX :: Float,
  m_worldVelocityY :: Float,
  m_worldVelocityZ :: Float,
  m_worldForwardDirX :: Int16,
  m_worldForwardDirY :: Int16,
  m_worldForwardDirZ :: Int16,
  m_worldRightDirX :: Int16,
  m_worldRightDirY :: Int16,
  m_worldRightDirZ :: Int16,
  m_gForceLateral :: Float,
  m_gForceLongitudinal :: Float,
  m_gForceVertical :: Float,
  m_yaw :: Float,
  m_pitch :: Float,
  m_roll :: Float
} deriving (Eq, Ord, Show)
{- BOILERPLATE CarMotion GetF1 -}
{- BOILERPLATE START -}
instance GetF1 CarMotion where
  getF1 = CarMotion <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

data Motion = Motion {
  m_carMotion :: List22 CarMotion,

  -- extra data just for the playerCar
  m_suspensionPosition :: List4 Float,
  m_suspensionVelocity :: List4 Float,
  m_suspensionAcceleration :: List4 Float,
  m_wheelSpeed :: List4 Float,
  m_wheelSlip :: List4 Float,
  m_localVelocityX :: Float,
  m_localVelocityY :: Float,
  m_localVelocityZ :: Float,
  m_angularVelocityX :: Float,
  m_angularVelocityY :: Float,
  m_angularVelocityZ :: Float,
  m_angularAccelerationX :: Float,
  m_angularAccelerationY :: Float,
  m_angularAccelerationZ :: Float,
  m_frontWheelsAngle :: Float
} deriving (Eq, Ord, Show)
{- BOILERPLATE Motion GetF1 -}
{- BOILERPLATE START -}
instance GetF1 Motion where
  getF1 = Motion <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

data MarshalZone = MarshalZone {
  m_zoneStart :: Float,
  m_zoneFlag :: Int8
} deriving (Eq, Ord, Show)
{- BOILERPLATE MarshalZone GetF1 -}
{- BOILERPLATE START -}
instance GetF1 MarshalZone where
  getF1 = MarshalZone <$> getF1 <*> getF1
{- BOILERPLATE END -}

data WeatherForecastSample = WeatherForecastSample {
  m_sessionTypeForecast :: Word8,
  m_timeOffset :: Word8,
  m_weatherForecast :: Word8,
  m_trackTemperatureForecast :: Int8,
  m_airTemperatureForecast :: Int8
} deriving (Eq, Ord, Show)
{- BOILERPLATE WeatherForecastSample GetF1 -}
{- BOILERPLATE START -}
instance GetF1 WeatherForecastSample where
  getF1 = WeatherForecastSample <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

data Session = Session {
  m_weather :: Word8,
  m_trackTemperature :: Int8,
  m_airTemperature :: Int8,
  m_totalLaps :: Word8,
  m_trackLength :: Word16,
  m_sessionType :: Word8,
  m_trackId :: Int8,
  m_formula :: Word8,
  m_sessionTimeLeft :: Word16,
  m_sessionDuration :: Word16,
  m_pitSpeedLimit :: Word8,
  m_gamePaused :: Word8,
  m_isSpectating :: Word8,
  m_spectatorCarIndex :: Word8,
  m_sliProNativeSupport :: Word8,
  m_numMarshalZones :: Word8,
  m_marshalZones :: List21 MarshalZone,
  m_safetyCarStatus :: Word8,
  m_networkGame :: Word8,
  m_numWeatherForecastSamples :: Word8,
  m_weatherForecastSamples :: List20 WeatherForecastSample
} deriving (Eq, Ord, Show)
{- BOILERPLATE Session GetF1 -}
{- BOILERPLATE START -}
instance GetF1 Session where
  getF1 = Session <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

-- could fix the names to be consistent "Name (Country)"
-- lookup for m_trackId
trackNames :: Map Int Text
trackNames =
  M.fromList $ indexed ["Melbourne", "Paul Ricard", "Shanghai", "Sakhir", "Catalunya", "Monaco", "Montreal", "Silverstone", "Hockenheim", "Hungaroring", "Spa", "Monza", "Singapore", "Suzuka", "Abu Dhabi", "Texas", "Brazil", "Austria", "Sochi", "Mexico", "Baku", "Sakhir Short", "Silverstone Short", "Texas Short", "Suzuka Short", "Hanoi", "Zandvoort"]

data CarLap = CarLap {
  m_lastLapTime :: Float,
  m_currentLapTime :: Float,
  m_sector1TimeInMS :: Word16,
  m_sector2TimeInMS :: Word16,
  m_bestLapTime :: Float,
  m_bestLapNum :: Word8,
  m_bestLapSector1TimeInMS :: Word16,
  m_bestLapSector2TimeInMS :: Word16,
  m_bestLapSector3TimeInMS :: Word16,
  m_bestOverallSector1TimeInMS :: Word16,
  m_bestOverallSector1LapNum  :: Word8,
  m_bestOverallSector2TimeInMS :: Word16,
  m_bestOverallSector2LapNum :: Word8,
  m_bestOverallSector3TimeInMS :: Word16,
  m_bestOverallSector3LapNum :: Word8,
  m_lapDistance :: Float,
  m_totalDistance :: Float,
  m_safetyCarDelta :: Float,
  m_carPosition :: Word8,
  m_currentLapNum :: Word8,
  m_pitStatus :: Word8,
  m_sector :: Word8,
  m_currentLapInvalid :: Word8,
  m_penalties :: Word8,
  m_gridPosition :: Word8,
  -- 0 = in garage, 1 = flying lap, 2 = in lap, 3 = out lap, 4 = on track
  m_driverStatus :: Word8,
  -- 0 = invalid, 1 = inactive, 2 = active, 3 = finished, 4 = disqualified, 5 = not classified, 6 = retired
  m_resultStatus :: Word8
} deriving (Eq, Ord, Show)
{- BOILERPLATE CarLap GetF1 -}
{- BOILERPLATE START -}
instance GetF1 CarLap where
  getF1 = CarLap <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

data Lap = Lap {
  m_lapData :: List22 CarLap
} deriving (Eq, Ord, Show)
{- BOILERPLATE Lap GetF1 -}
{- BOILERPLATE START -}
instance GetF1 Lap where
  getF1 = Lap <$> getF1
{- BOILERPLATE END -}

data CarParticipant = CarParticipant {
  -- PUNY_HUMAN(0) / AI(1)
  m_aiControlled :: Word8,
  m_driverId :: Word8,
  m_teamId :: Word8,
  m_raceNumber :: Word8,
  m_nationality :: Word8,
  m_name :: Text48,
  m_yourTelemetry :: Word8
} deriving (Eq, Ord, Show)
{- BOILERPLATE CarParticipant GetF1 -}
{- BOILERPLATE START -}
instance GetF1 CarParticipant where
  getF1 = CarParticipant <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

data Participants = Participants {
  m_numActiveCars :: Word8,
  m_participants :: List22 CarParticipant
} deriving (Eq, Ord, Show)
{- BOILERPLATE Participants GetF1 -}
{- BOILERPLATE START -}
instance GetF1 Participants where
  getF1 = Participants <$> getF1 <*> getF1
{- BOILERPLATE END -}

data CarInput = CarInput {
  m_speed :: Word16,
  m_throttle :: Float,
  m_steer :: Float,
  m_brake :: Float,
  m_clutch :: Word8,
  m_gear :: Int8,
  m_engineRPM :: Word16,
  m_drs :: Word8,
  m_revLightsPercent :: Word8,
  m_brakesTemperature :: List4 Word16,
  m_tyresSurfaceTemperature :: List4 Word8,
  m_tyresInnerTemperature :: List4 Word8,
  m_engineTemperature :: Word16,
  m_tyresPressure :: List4 Float,
  m_surfaceType :: List4 Word8
} deriving (Eq, Ord, Show)
{- BOILERPLATE CarInput GetF1 -}
{- BOILERPLATE START -}
instance GetF1 CarInput where
  getF1 = CarInput <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}

data Input = Input {
  m_carTelemetryData :: List22 CarInput,
  m_buttonStatus :: Word32,
  m_mfdPanelIndex :: Word8,
  m_mfdPanelIndexSecondaryPlayer :: Word8,
  m_suggestedGear :: Int8
} deriving (Eq, Ord, Show)
{- BOILERPLATE Input GetF1 -}
{- BOILERPLATE START -}
instance GetF1 Input where
  getF1 = Input <$> getF1 <*> getF1 <*> getF1 <*> getF1 <*> getF1
{- BOILERPLATE END -}
