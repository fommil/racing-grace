{-# LANGUAGE CPP #-}

module Main where

import Control.Concurrent (forkIO)
import Control.Concurrent.BoundedChan (newBoundedChan)
import Control.Monad (void, when)
import Data.Maybe (fromMaybe, listToMaybe)
import RacingGrace.F1Parser (runF1Parser)
import RacingGrace.Recorder (runRecorder)
import RacingGrace.Replayer (runReplayer)
import RacingGrace.Udp
import System.Environment (getArgs)
import System.Exit (ExitCode(..), exitWith)
import System.FilePath (addTrailingPathSeparator)

version :: String
#ifdef CURRENT_PACKAGE_VERSION
version = CURRENT_PACKAGE_VERSION
#else
version = "unknown"
#endif

help :: String
help =
  "racing-grace [GLOBAL FLAGS]\n\n" ++
  "Global flags:\n\n" ++
  " -h,--help               Show this help text\n" ++
  " -v,--version            Print version information\n" ++
  "    --record PATH        Record raw packets to PATH (disables processing)\n" ++
  "    --replay PATH        Replay raw packets from PATH (disables udp server)\n" ++
  "    --port PORT          Listen on port PORT (default " <> defaultPort <> ")\n" ++
  "    --verbose            Print debugging information to stderr\n"

defaultPort :: String
defaultPort = "20777"

main :: IO ()
main = do
  args <- getArgs
  let hasAny :: Eq a => [a] -> [a] -> Bool
      hasAny [] _ = False
      hasAny _ [] = False
      hasAny as (x : xs) | elem x as = True
                         | otherwise = hasAny as xs

  when (hasAny ["-h", "--help"] args) $
    (putStrLn help) >> exitWith ExitSuccess
  when (hasAny ["-v", "--version"] args) $
    (putStrLn version) >> exitWith ExitSuccess

  let named p = listToMaybe $ findAfter p args
      record_path = fmap addTrailingPathSeparator $ named "--record"
      replay_path = fmap addTrailingPathSeparator $ named "--replay"
      port = read . fromMaybe defaultPort $ named "--port"

  q <- newBoundedChan 10000

  void . forkIO $ case replay_path of
    Nothing -> do
      putStrLn $ "Starting Racing Grace on port " <> show port
      runConsumer q port

    Just path -> do
      putStrLn $ "Replaying Racing Grace data from " <> path
      runReplayer q path

  case record_path of
    Nothing -> do
      putStrLn $ "Starting processing"
      telemetry <- newBoundedChan 10000
      runF1Parser q telemetry

    Just path -> do
      putStrLn $ "Starting Recording to " <> show path
      runRecorder q path

findAfter :: Eq a => a -> [a] -> [a]
findAfter _ [] = []
findAfter a (a' : as) = if (a == a') then as else findAfter a as
